﻿using System.Threading.Tasks;
using MarketingInvoice.Infrastructure.Administrator;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace MarketingInvoice.WebAPI.Controllers.Administrator
{
    [Route(AdministratorRotues.Root)]
    [ApiController]
    public class AdministratorController : ControllerBase
    {
        private readonly IMediator mediator;
        public AdministratorController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet(AdministratorRotues.RepaymentSchedule)]
        public async Task<ActionResult> GetRepaymentSchedule([FromQuery]GetRepaymentSchedule.Request request)
        {
            var repaymentSchedule = await mediator.Send(request);

            return Ok(repaymentSchedule);
        }
    }
}