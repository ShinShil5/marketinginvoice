﻿using System.Threading.Tasks;
using MarketingInvoice.Infrastructure.Customer;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace MarketingInvoice.WebAPI.Controllers.Customer
{
    [Route(CustomerRoutes.Root)]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator mediator;

        public CustomerController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet(CustomerRoutes.LoansSummary)]
        public async Task<ActionResult> GetLoanSummary([FromQuery]GetLoanSummary.Request request)
        {
            var loanSummary = await mediator.Send(request);

            return Ok(loanSummary);
        }
    }
}