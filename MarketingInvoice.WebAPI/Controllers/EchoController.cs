﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketingInvoice.Infrastructure.Echo;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace MarketingInvoice.WebAPI.Controllers
{
    /**
     * Sample controller, it used to check that environment working as expected
     * Thi Echo demonstrates how mediator, validation and DI should be used in the application
     */
    [Route("api/echo")]
    [ApiController]
    public class EchoController : ControllerBase
    {
        private readonly IMediator mediator;

        public EchoController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async Task<ActionResult> Echo([FromQuery]Echo.Request request)
        {
            var result = await mediator.Send(request);

            return Ok(result);
        }

    }
}
