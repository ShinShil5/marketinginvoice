# MarketInvoice Coding Exercise

At MarketInvoice, we'd like to offer our customer loans. We would like to inform them how much a weekly installment will be, the total amount of interest paid and the overall cost of the loan.

For accounting purposes we need to know how much of each repayment is paying off interest and how much is paying off the principal.



##  The scope of the exercise is as follows:

We would like you to write a loans calculator API with two endpoints returning json fulfilling the following user stories:

1. As a prospective loan customer, I want to know the weekly instalment, the total cost and the amount of interest paid for a loan of a given amount and APR.

2. As a loans administrator, I want to know what the breakdown of repayments are in terms of interest paid and principal repaid for a given loan amount and APR.