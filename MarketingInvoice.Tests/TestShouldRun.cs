using MarketingInvoice.Core.Echo;
using System;
using Xunit;

namespace MarketingInvoice.Tests
{
    public class TestShouldPass
    {
        [Fact]
        public void ShouldPass()
        {
            Assert.True(true);
        }

        [Theory]
        [InlineData("echo", "echo")]
        public void EchoServiceWorking(string input, string expected)
        {
            var echoService = new EchoService();
            var echoString = echoService.Echo(input);

            Assert.True(echoString == expected);
        }
    }
}
