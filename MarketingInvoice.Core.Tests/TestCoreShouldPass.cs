using MarketingInvoice.Core.Echo;
using Xunit;

namespace MarketingInvoice.Core.Tests
{
    public class TestCoreShouldPass
    {
        [Fact]
        public void ShouldPass()
        {
            Assert.True(true);
        }

        [Theory]
        [InlineData("echo", "echo")]
        public void EchoServiceWorking(string input, string expected)
        {
            var echoService = new EchoService();
            var echoString = echoService.Echo(input);

            Assert.True(echoString == expected);
        }
    }
}
