﻿using MarketingInvoice.Core.LoansCalculator;
using System;
using Xunit;

namespace MarketingInvoice.Tests.Core.TestLoansCalculator
{
    public class TestLoansCalculator
    {
        private readonly ILoansCalculator loansCalculator;
        private const int Precision = 3;
        public TestLoansCalculator()
        {
            loansCalculator = new LoansCalculator();
        }

        [Theory]
        [InlineData(50000, 0.19, 52, 1057.526)]
        [InlineData(80000, 0.19, 52, 1692.041)]
        [InlineData(70000, 0.25, 50, 1585.763)]
        public void GetPeriodRepaimentReturnsExpected(double amount, double apr, int numberOfPeriods, double expectedValue)
        {
            var periodRepaiment = loansCalculator.GetPeriodRepayment(amount, apr, numberOfPeriods);

            Assert.True(Round(periodRepaiment) == expectedValue);
        }

        [Theory]
        [InlineData(50000, 0.19, 52, 54991.343)]
        [InlineData(80000, 0.19, 52, 87986.149)]
        [InlineData(70000, 0.25, 50, 79288.153)]
        public void GetTotalRepaidReturnsExpected(double amount, double apr, int numberOfPeriods, double expectedValue)
        {
            var totalRepaid = loansCalculator.GetTotalRepaid(amount, apr, numberOfPeriods);

            Assert.True(Round(totalRepaid) == expectedValue);
        }

        [Theory]
        [InlineData(50000, 0.19, 52, 4991.343)]
        [InlineData(80000, 0.19, 52, 7986.149)]
        [InlineData(70000, 0.25, 50, 9288.153)]
        public void GetTotalInterestReturnsExpected(double amount, double apr, int numberOfPeriods, double expectedValue)
        {
            var totalInterest = loansCalculator.GetTotalInterest(amount, apr, numberOfPeriods);

            Assert.True(Round(totalInterest) == expectedValue);
        }

        [Theory]
        [InlineData(5, 5, 25)]
        [InlineData(2, 3, 6)]
        [InlineData(0, 0, 0)]
        public void GetTotalRepaidFromRepaymentReturnsExpecetd(double periodRepayment, int numberOfPeriods, double expectedValue)
        {
            var totalRepaid = loansCalculator.GetTotalRepaid(periodRepayment, numberOfPeriods);

            Assert.True(Round(totalRepaid) == expectedValue);
        }

        [Theory]
        [InlineData(5, 5, 0)]
        [InlineData(7, 9, 2)]
        [InlineData(0, 0, 0)]
        public void GetTotalInterestFromTotalRepaymentReturnsExpecetd(double amount, double totalRepayment, double expectedValue)
        {
            var totalInterest = loansCalculator.GetTotalInterest(amount, totalRepayment);

            Assert.True(Round(totalInterest) == expectedValue);
        }

        [Theory]
        [InlineData(0, 5, 0)]
        [InlineData(10, 5, 2)]
        [InlineData(0.5, 1, 0.5)]
        public void GetInstallmentPeriodInterestRateReturnsExpected(double apr, int numberOfPeriods, double expectedValue)
        {
            var installmentPeriodInterestRate = loansCalculator.GetInstallmentPeriodInterestRate(apr, numberOfPeriods);

            Assert.True(Round(installmentPeriodInterestRate) == expectedValue);
        }

        [Theory]
        [InlineData(5, 5, 25)]
        [InlineData(2, 3, 6)]
        [InlineData(0, 0, 0)]
        public void GetPeriodInterestReturnsExpected(double installmentPeriodInterestRate, double numberOfPeriods, double expectedValue)
        {
            var periodInterest = loansCalculator.GetPeriodInterest(installmentPeriodInterestRate, numberOfPeriods);

            Assert.True(periodInterest == expectedValue);
        }

        [Theory]
        [InlineData(5, 5, 0)]
        [InlineData(9, 7, 2)]
        [InlineData(0, 0, 0)]
        public void GetPeriodPrincipalReturnsExpected(double totalRepayment, double periodInterest, double expectedValue)
        {
            var periodPrincipal = loansCalculator.GetPeriodPrincipal(totalRepayment, periodInterest);

            Assert.True(periodPrincipal == expectedValue);
        }

        private double Round(double value) => Math.Round(value, Precision);
    }
}
