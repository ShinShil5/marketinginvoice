﻿namespace MarketingInvoice.Core.LoansCalculator
{
    public interface ILoansCalculator
    {
        double GetPeriodRepayment(double amount, double apr, int numberOfPeriods);
        double GetTotalRepaid(double amount, double apr, int numberOfPeriods);
        double GetTotalRepaid(double periodRepayment, int numberOfPeriods);
        double GetTotalInterest(double amount, double apr, int numberOfPeriods);
        double GetTotalInterest(double amount, double totalRepayment);
        double GetPeriodInterest(double installmentPeriodInterestRate, double numberOfPeriods);
        double GetInstallmentPeriodInterestRate(double apr, int numberOfPeriods);
        double GetPeriodPrincipal(double totalRepayment, double periodInterest);
    }
}
