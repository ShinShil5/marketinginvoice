﻿using System;

namespace MarketingInvoice.Core.LoansCalculator
{
    public class LoansCalculator : ILoansCalculator
    {
        public double GetPeriodRepayment(double amount, double apr, int numberOfPeriods)
        {
            var r = GetInstallmentPeriodInterestRate(apr, numberOfPeriods);
            var divider = 1 - Math.Pow(1 + r, -numberOfPeriods);
            var periodRepaiment = amount * (r / divider);

            return periodRepaiment;
        }

        public double GetTotalInterest(double amount, double apr, int numberOfPeriods)
        {
            var totalRepaid = GetTotalRepaid(amount, apr, numberOfPeriods);
            var totalInterest = totalRepaid - amount;

            return totalInterest;
        }

        public double GetTotalInterest(double amount, double totalRepayment)
        {
            var totalInterest = totalRepayment - amount;

            return totalInterest;
        }

        public double GetTotalRepaid(double amount, double apr, int numberOfPeriods)
        {
            var periodRepayment = GetPeriodRepayment(amount, apr, numberOfPeriods);
            var totalRepaid = GetTotalRepaid(periodRepayment , numberOfPeriods);

            return totalRepaid;
        }

        public double GetTotalRepaid(double periodRepayment, int numberOfPeriods)
        {
            var totalRepaid = periodRepayment * numberOfPeriods;

            return totalRepaid; 
        }

        public double GetInstallmentPeriodInterestRate(double apr, int numberOfPeriods)
        {
            var installmentPeriodInterestRate = apr / numberOfPeriods;

            return installmentPeriodInterestRate;
        }
        
        public double GetPeriodInterest(double installmentPeriodInterestRate, double numberOfPeriods)
        {
            var periodInterest = installmentPeriodInterestRate * numberOfPeriods;

            return periodInterest;
        }

        public double GetPeriodPrincipal(double totalRepayment, double periodInterest)
        {
            var periodPrincipal = totalRepayment - periodInterest;

            return periodPrincipal;
        }
    }
}
