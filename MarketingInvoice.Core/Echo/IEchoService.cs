﻿namespace MarketingInvoice.Core.Echo
{
    public interface IEchoService
    {
        string Echo(string echoString);
    }
}
