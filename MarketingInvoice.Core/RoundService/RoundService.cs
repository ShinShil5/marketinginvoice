﻿using System;

namespace MarketingInvoice.Core.RoundService
{
    public class RoundService : IRoundService
    {
        public double ToDouble(double value, int precision = RoundConstants.DefaultPrecision)
        {
            return Math.Round(value, precision);
        }

        public int ToInt(double value)
        {
            return (int)Math.Round(value);
        }
    }
}
