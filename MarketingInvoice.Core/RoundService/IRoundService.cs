﻿namespace MarketingInvoice.Core.RoundService
{
    public interface IRoundService
    {
        double ToDouble(double value, int precision);

        int ToInt(double value);
    }
}
