﻿namespace MarketingInvoice.Infrastructure
{
    public class InfrastructureConstants
    {
        public const int DefaultPeriod = 52;
        public const int RepaymentScheduleAmountDuePrecision = 2;
        public const int OneHundred = 100;
    }
}
