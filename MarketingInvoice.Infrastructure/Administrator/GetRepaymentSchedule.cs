﻿using MarketingInvoice.Core.LoansCalculator;
using MarketingInvoice.Core.RoundService;
using MarketingInvoice.Infrastructure.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MarketingInvoice.Infrastructure.Administrator
{
    public class GetRepaymentSchedule
    {
        public class Request : BaseLoanApiDto, IRequest<Result>
        {
        }

        public class Result
        {
            public IEnumerable<Installment> Installments { get; set; }
        }

        public class Handler : IRequestHandler<Request, Result>
        {
            private readonly ILoansCalculator loansCalculator;
            private readonly IRoundService roundService;

            public Handler(ILoansCalculator loansCalculator, IRoundService roundService)
            {
                this.loansCalculator = loansCalculator;
                this.roundService = roundService;
            }

            public Task<Result> Handle(Request request, CancellationToken cancellationToken)
            {
                var installments = new List<Installment>();

                var amount = request.Amount;
                var apr = request.AprNumber;
                var currentBalance = request.Amount;
                var installmentPeriodInterestRate = loansCalculator.GetInstallmentPeriodInterestRate(apr, InfrastructureConstants.DefaultPeriod);
                var periodRepayment = loansCalculator.GetPeriodRepayment(amount, apr, InfrastructureConstants.DefaultPeriod);
                for(int i = 0; i<InfrastructureConstants.DefaultPeriod; ++i)
                {
                    var installmentNumber = i + 1;
                    var installmentDouble = GetInstallment(installmentNumber, currentBalance, installmentPeriodInterestRate, periodRepayment);
                    currentBalance -= installmentDouble.Principal;
                    var installment = new Installment(installmentDouble, roundService);

                    installments.Add(installment);
                }

                var repaymentSchedule = new Result()
                {
                    Installments = installments
                };

                return Task.FromResult(repaymentSchedule);
            }

            private InstallmentDouble GetInstallment(int installmentNumber, double currentBalance, double installmentPeriodInterestRate,  double periodRepayment)
            {
                var periodInterest = loansCalculator.GetPeriodInterest(installmentPeriodInterestRate, currentBalance);
                var periodPrincipal = loansCalculator.GetPeriodPrincipal(periodRepayment, periodInterest);
                var installment = new InstallmentDouble()
                {
                    AmountDue = currentBalance,
                    InstallmentNumber = installmentNumber,
                    Interest = periodInterest,
                    Principal = periodPrincipal
                };

                return installment;
            }
        }

        public class InstallmentDouble
        {
            public int InstallmentNumber { get; set; }
            public double AmountDue { get; set; }
            public double Principal { get; set; }
            public double Interest { get; set; }

        }
        public class Installment
        {
            public Installment()
            {

            }

            public Installment(InstallmentDouble installment, IRoundService roundService)
            {
                InstallmentNumber = installment.InstallmentNumber;
                AmountDue = roundService.ToDouble(installment.AmountDue, InfrastructureConstants.RepaymentScheduleAmountDuePrecision);
                Principal = roundService.ToInt(installment.Principal);
                Interest = roundService.ToInt(installment.Interest);
            }
            public int InstallmentNumber { get; set; }
            public double AmountDue { get; set; }
            public int Principal { get; set; }
            public int Interest { get; set; }
        }
    }
}
