﻿using MarketingInvoice.Core.LoansCalculator;
using MarketingInvoice.Core.RoundService;
using MarketingInvoice.Infrastructure.Models;
using MediatR;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace MarketingInvoice.Infrastructure.Customer
{
    public class GetLoanSummary
    {
        public class Request : BaseLoanApiDto, IRequest<Result>
        {
        }

        public class Result
        {
            public int WeeklyRepayment { get; set; }
            public int TotalRepaid { get; set; }
            public int TotalInterest { get; set; }
        }

        public class Handler : IRequestHandler<Request, Result>
        {
            private readonly ILoansCalculator loansCalculator;
            private readonly IRoundService roundService;

            public Handler(ILoansCalculator loansCalculator, IRoundService roundService)
            {
                this.loansCalculator = loansCalculator;
                this.roundService = roundService;
            }

            public Task<Result> Handle(Request request, CancellationToken cancellationToken)
            {
                var amount = request.Amount;
                var apr = request.AprNumber;
                var weeklyRepayment = loansCalculator.GetPeriodRepayment(amount, apr, InfrastructureConstants.DefaultPeriod);
                var totalRepaid = loansCalculator.GetTotalRepaid(roundService.ToInt(weeklyRepayment), InfrastructureConstants.DefaultPeriod);
                var totalInterest = loansCalculator.GetTotalInterest(amount, roundService.ToInt(totalRepaid));
                var loanSummary = new Result
                {
                    TotalInterest = roundService.ToInt(totalInterest),
                    TotalRepaid = roundService.ToInt(totalRepaid),
                    WeeklyRepayment = roundService.ToInt(weeklyRepayment)
                };

                return Task.FromResult(loanSummary);
            }
        }
    }
}
