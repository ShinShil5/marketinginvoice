﻿using System.ComponentModel.DataAnnotations;

namespace MarketingInvoice.Infrastructure.Models
{
    public class BaseLoanApiDto
    {
        [Required]
        [Range(0.0, double.MaxValue)]
        public double Amount { get; set; }

        [Required]
        [Range(0.000001, 100.0)]
        public double Apr { get; set; }

        public double AprNumber
        {
            get
            {
                return Apr / InfrastructureConstants.OneHundred;
            }
        }
    }
}
