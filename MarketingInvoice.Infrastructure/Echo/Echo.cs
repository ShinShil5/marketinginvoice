﻿using MarketingInvoice.Core.Echo;
using MediatR;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace MarketingInvoice.Infrastructure.Echo
{
    public class Echo
    {
        public class Request : IRequest<Result>
        {
            [Required]
            public string EchoString { get; set; }
        }

        public class Result
        {
            public string EchoString { get; set; }
        }

        public class Handler : IRequestHandler<Request, Result>
        {
            private readonly IEchoService echoService;

            public Handler(IEchoService echoService)
            {
                this.echoService = echoService;
            }

            public Task<Result> Handle(Request request, CancellationToken cancellationToken)
            {
                var echoString = echoService.Echo(request.EchoString);
                var result = new Result
                {
                    EchoString = echoString
                };

                return Task.FromResult(result);
            }
        }
    }
}
