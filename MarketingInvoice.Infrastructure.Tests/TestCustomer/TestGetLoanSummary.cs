﻿using MarketingInvoice.Core.LoansCalculator;
using MarketingInvoice.Core.RoundService;
using MarketingInvoice.Infrastructure.Customer;
using System.Threading.Tasks;
using Xunit;

namespace MarketingInvoice.Infrastructure.Tests.TestCustomer
{
    public class TestGetLoanSummary
    {
        [Fact]
        public async Task GetLoanSummaryReturnsExpected()
        {
            var request = new GetLoanSummary.Request
            {
                Amount = 50000,
                Apr = 19
            };

            var expectedLoanSummary = new GetLoanSummary.Result
            {
                TotalInterest = 5016,
                TotalRepaid = 55016,
                WeeklyRepayment = 1058
            };

            var loanCalculator = new LoansCalculator();
            var roundService = new RoundService();
            var handler = new GetLoanSummary.Handler(loanCalculator, roundService);
            var loanSummary = await handler.Handle(request, new System.Threading.CancellationToken());

            Assert.True(loanSummary.TotalInterest == expectedLoanSummary.TotalInterest, $"{nameof(loanSummary.TotalInterest)} is the same");
            Assert.True(loanSummary.TotalRepaid == expectedLoanSummary.TotalRepaid, $"{nameof(loanSummary.TotalRepaid)} is the same");
            Assert.True(loanSummary.WeeklyRepayment == expectedLoanSummary.WeeklyRepayment, $"{nameof(loanSummary.WeeklyRepayment)} is the same");
        }
    }
}
