﻿using MarketingInvoice.Core.LoansCalculator;
using MarketingInvoice.Core.RoundService;
using MarketingInvoice.Infrastructure.Administrator;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Xunit;

namespace MarketingInvoice.Infrastructure.Tests.TestAdministrator
{
    public class TestGetRepaymentSchedule
    {
        [Fact]
        public async Task GetRepaymentScheduleReturnsExpected()
        {
            var request = new GetRepaymentSchedule.Request
            {
                Amount = 50000,
                Apr = 19
            };

            var expectedInstallments = new List<GetRepaymentSchedule.Installment>() {
                GetInstallment(1, 50000, 875, 183),
                GetInstallment(2, 49125.17, 878, 179),
                GetInstallment(10, 42010.44, 904, 153),
                GetInstallment(26, 27142.8, 958, 99),
                GetInstallment(52, 1053.68, 1054, 4)
            };


            var loanCalculator = new LoansCalculator();
            var roundService = new RoundService();
            var handler = new GetRepaymentSchedule.Handler(loanCalculator, roundService);
            var repaymentSchedule = await handler.Handle(request, new System.Threading.CancellationToken());

            foreach (var expectedInstallment in expectedInstallments)
            {
                var installment = repaymentSchedule.Installments.FirstOrDefault(it => it.InstallmentNumber == expectedInstallment.InstallmentNumber);

                Assert.True(installment != null, GetInstallmentErrorMessage("installment was not found", expectedInstallment));
                Assert.True(expectedInstallment.AmountDue - installment.AmountDue == 0, GetInstallmentErrorMessage($"{nameof(expectedInstallment.AmountDue)} should be the same", expectedInstallment));
                Assert.True(expectedInstallment.Interest == installment.Interest, GetInstallmentErrorMessage($"{nameof(expectedInstallment.Interest)} should be the same", expectedInstallment));
                Assert.True(expectedInstallment.Principal == installment.Principal, GetInstallmentErrorMessage($"{nameof(expectedInstallment.Principal)} should be the same", expectedInstallment));
            }
        }

        private string GetInstallmentErrorMessage(string message, GetRepaymentSchedule.Installment installment)
        {
            var errorMessage = $"installment number {installment.InstallmentNumber}: {message}";

            return errorMessage;
        }

        private GetRepaymentSchedule.Installment GetInstallment(int installmentNumber, double amountDue, int principal, int interest)
        {
            var installment = new GetRepaymentSchedule.Installment
            {
                InstallmentNumber = installmentNumber,
                AmountDue = amountDue,
                Principal = principal,
                Interest = interest
            };

            return installment;
        }
    }
}
